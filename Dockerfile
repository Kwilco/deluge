FROM ubuntu:18.04

RUN apt update \
    && apt-get install software-properties-common --yes \
    && apt-get update \
    && add-apt-repository ppa:deluge-team/stable --yes \
    && apt-get update \
    && apt-get install deluged deluge-web --yes

VOLUME /config
VOLUME /downloads

EXPOSE 58846

CMD bash -c "deluge-web && deluged --do-not-daemonize --config /config --loglevel info"
